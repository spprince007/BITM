-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 26, 2017 at 05:32 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shop_seip_171759`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `phone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `name`, `amount`, `phone`) VALUES
(1, 'Prince', 11000, 1911510513),
(3, 'frahad', 2560, 1929422127),
(4, 'Aslam Biswas', 1200, 1911704730),
(6, 'Polash', 36598, 19568926),
(7, 'Rakib', 25698, 1911587632);

-- --------------------------------------------------------

--
-- Table structure for table `products_cat`
--

CREATE TABLE `products_cat` (
  `id` int(11) NOT NULL,
  `biskit` varchar(255) NOT NULL,
  `drinks` varchar(255) NOT NULL,
  `fastfood` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `products_cat`
--

INSERT INTO `products_cat` (`id`, `biskit`, `drinks`, `fastfood`) VALUES
(1, 'dry cake', '7up', 'chamuca'),
(2, 'choklet biskit', 'fanda', 'burger'),
(3, 'alter solt', 'mirinda', 'puri'),
(4, 'tost', 'sprite', 'fuska');

-- --------------------------------------------------------

--
-- Table structure for table `products_details`
--

CREATE TABLE `products_details` (
  `id` int(11) NOT NULL,
  `pc_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `expried_date` date NOT NULL,
  `quntity` int(11) NOT NULL,
  `sells_price` int(11) NOT NULL,
  `buy_price` int(11) NOT NULL,
  `brand` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `products_details`
--

INSERT INTO `products_details` (`id`, `pc_id`, `price`, `expried_date`, `quntity`, `sells_price`, `buy_price`, `brand`) VALUES
(1, 1, 450, '2017-08-31', 10, 560, 450, 'pran'),
(2, 2, 650, '2017-08-29', 3, 780, 650, 'ruci'),
(3, 3, 890, '2017-08-31', 5, 1250, 890, 'lichi'),
(4, 4, 320, '2017-08-28', 14, 500, 320, 'maggi'),
(5, 5, 360, '2017-09-01', 25, 680, 360, 'cocola'),
(6, 6, 8960, '2017-09-15', 120, 10000, 8960, 'rfl');

-- --------------------------------------------------------

--
-- Table structure for table `suplier`
--

CREATE TABLE `suplier` (
  `id` int(11) NOT NULL,
  `name` varchar(11) NOT NULL,
  `area` varchar(11) NOT NULL,
  `phone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suplier`
--

INSERT INTO `suplier` (`id`, `name`, `area`, `phone`) VALUES
(1, 'kamal', 'dhanmondi', 195689548),
(2, 'noyon', 'dhaka uddan', 1911896989),
(3, 'manjarul', 'md pur', 169895645),
(4, 'bikul', 'monoherpur', 1952420420);

-- --------------------------------------------------------

--
-- Table structure for table `tranjection`
--

CREATE TABLE `tranjection` (
  `id` int(11) NOT NULL,
  `pd_id` int(11) NOT NULL,
  `cd_id` int(11) NOT NULL,
  `s_id` int(11) NOT NULL,
  `t_amount` int(11) NOT NULL,
  `d_amount` int(11) NOT NULL,
  `staus` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tranjection`
--

INSERT INTO `tranjection` (`id`, `pd_id`, `cd_id`, `s_id`, `t_amount`, `d_amount`, `staus`) VALUES
(1, 1, 1, 1, 25000, 14000, 'some tk due'),
(2, 2, 3, 2, 2560, 0, 'Done'),
(3, 3, 4, 3, 2400, 1200, '50% done'),
(4, 4, 6, 4, 50000, 24402, '70% done'),
(5, 5, 7, 5, 30000, 24302, '62% done');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_cat`
--
ALTER TABLE `products_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_details`
--
ALTER TABLE `products_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suplier`
--
ALTER TABLE `suplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tranjection`
--
ALTER TABLE `tranjection`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `products_cat`
--
ALTER TABLE `products_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `products_details`
--
ALTER TABLE `products_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `suplier`
--
ALTER TABLE `suplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tranjection`
--
ALTER TABLE `tranjection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
